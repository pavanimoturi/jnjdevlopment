import React from 'react';
import { Button } from 'reactstrap';
import Routing from './components/routing';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
        <Routing />
      </div>
  );
}

export default App;
