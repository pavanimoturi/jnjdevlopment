import React from 'react'
import Moment from 'react-moment';
import {Link} from 'react-router-dom';

import {
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Collapse,
  Table
} from 'reactstrap';
import {Pagination, PaginationItem, PaginationLink} from 'reactstrap';

import {getModels, getUtterances} from '../../../services/luis.service'

import "./modelView.css";

class Utterances extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: true,
      examples: [],
      page: 1,
      step: 10,
    }

  }

  componentDidMount() {}

  toggle = () => {
    this.setState({
      collapse: !this.state.collapse
    });
    if (this.state.collapse) {
      getUtterances(this.props.model, this.props.intent).then(response => response.json()).then(data => {
        console.log(data)
        this.setState({examples: data, loading: false})
        console.log(this.state.examples.length / 10)
      });
    }
  }

  selectPage = (pageNumber) => {
    this.setState({
      page : pageNumber
    });
    console.log(this.state.page);
  }

  render() {
    return (<ListGroupItem key={this.props.intent.id} className="pb-4">
      <div className="row pointer" onClick={this.toggle}>
        <div className="col-4">
          <span className="text-extra-muted uppercase extra-small weight-400">Intent</span>
          <br/>
          <span className="fs-20">{this.props.intent.name}</span>
        </div>
        <div className="col-4">
          <span className="text-extra-muted uppercase extra-small weight-400">Examples</span>
          <br/>
          <span className="fs-20">19</span>
        </div>
        <div className="col-3">
          <span className="text-extra-muted uppercase extra-small weight-400">ID</span>
          <br/>
          <span className="small">{this.props.intent.id}</span>
        </div>
        <div className="col-1 pt-4 text-muted">
          {
            !this.state.collapse
              ? (<span className="fa fa-fw fa-caret-down"></span>)
              : (<span className="fa fa-fw fa-caret-right"></span>)
          }

        </div>
      </div>
      <Collapse isOpen={!this.state.collapse}>
        <hr className="mt-5 mb-4"/>
        <div className="mb-4">
          <span className="weight-600 fs-15">
            Available Utterance Examples
          </span>
        </div>
        <div className="examples-list-wrapper">
          {
            this.state.examples.length > 0
              ? (<div>
                <Table>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th className="utterance-text">Utterance</th>
                      <th>Created On</th>
                      <th>Last Modified</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.examples.slice(((this.state.page * this.state.step) - this.state.step), (this.state.page * this.state.step)).map((example, index) => (<tr key={example.id}>
                        <th scope="row">
                          {((this.state.page * this.state.step) - this.state.step) + index + 1}
                        </th>
                        <td className="utterance-text">{example.text}</td>
                        <td className="extra-small">
                          <Moment format="MMM DD, YYYY hh:mm A">
                            {example.createdDateTime}
                          </Moment>
                        </td>
                        <td className="extra-small">
                          <Moment format="MMM DD, YYYY hh:mm A">
                            {example.modifiedDateTime}
                          </Moment>
                        </td>
                      </tr>))
                    }

                  </tbody>
                </Table>

                <div className="float-right">
                  <Pagination aria-label="Page navigation example">

                    <PaginationItem>
                      <PaginationLink previous="previous" href="#"/>
                    </PaginationItem>
                    {
                      Array.from(Array(Math.ceil(this.state.examples.length / this.state.step)), (elem, index) => {
                        return (
                          <PaginationItem key={index}>
                            <PaginationLink onClick={(event) => this.selectPage(index+1, event)}>
                              {index+1}
                            </PaginationLink>
                          </PaginationItem>
                        )
                      })
                    }

                    <PaginationItem>
                      <PaginationLink next="next" href="#"/>
                    </PaginationItem>

                  </Pagination>

                </div>
              </div>)
              : (<div className="text-center mb-5">
                  <span className="fs-25 text-muted fa fa-fw fa-gear fa-spin"></span>
                </div>)

          }
        </div>
      </Collapse>
    </ListGroupItem>)
  }
}
export default Utterances
