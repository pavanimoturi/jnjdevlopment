import React from "react";
import Moment from "react-moment";
import {Link, withRouter} from "react-router-dom";

import {getModels, dispatchRefresh} from '../../services/luis.service'

import "./luisModels.css";

class LuisModels extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      models: []
    };
  }

  componentDidMount() {
    getModels()
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({models: data, loading: false});
      });
  }

  render() {
    return (
      <section className="luis-models">
        <div className="container mt-5">
          <div>
            <div className="section-name-wrapper weight-700">
              LUIS Models
              <span className="weight-400">&nbsp;Management</span>
            </div>
            <div className="">Build a bot dialog model and integrate it with your APIs.</div>
            <div className="separator mt-4 w-25">
              <hr />
            </div>
          </div>
        </div>

        {this.state.loading ? (
          <div className="container">
            <div className="text-center text-muted mt-5">
              <div className="not-fonund weight-400 fs-30">
                <span className="fa fa-fw fa-cog fa-spin" />
                <br />
                Fetching your models
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="create-new-button-row mt-4">
              <button className="btn btn-outline-secondary button-primary">
                <span className="fa fa-fw fa-plus" />
                Create New Q&A Scenario
              </button>
              <button className="btn btn-outline-primary button-secondary ml-2" onClick={dispatchRefresh}>
                <span className="fa fa-fw fa-refresh" />
                Refresh Bot Dispatch
              </button>
            </div>
            <div className="mt-4">
              <div className="list-headers">
                <div className="row models-row mb-2">
                  {this.state.models.map((model, index) => (
                    <div className="col-4 mb-4" key={model.id}>
                      <div className="card model-card">
                        <div className="card-body">
                          <Link
                            to={{
                              pathname: "/luis-models/" + model.id,
                              state: model
                            }}>
                            <div className="card-body-inner pointer">
                              <div className="extra-small uppercase text-extra-muted scenario-number weight-400">Model {index + 1}</div>
                              <div className="model-name qna-model-name weight-700 pointer smoothen-transition">
                                <span>{model.name}</span>
                              </div>
                              <div className="w-50">
                                <hr />
                              </div>
                              <div className="row">
                                <div className="col-6">
                                  <div className="model-info">
                                    <strong className="text-extra-muted small weight-600">Created On</strong>
                                    <br />
                                    <small>
                                      <Moment className="" format="MMM DD, YYYY, hh:mm A">
                                        {model.createdDateTime}
                                      </Moment>
                                    </small>
                                  </div>
                                </div>
                                <div className="col-6">
                                  <div className="model-info">
                                    <strong className="text-extra-muted small weight-600">Endpoint Hits</strong>
                                    <br />
                                    <small>{model.endpointHitsCount}</small>
                                  </div>
                                </div>
                              </div>
                              <div className="model-info mt-3">
                                <strong className="text-extra-muted small weight-600">Description</strong>
                                <br /> {model.description}
                              </div>
                            </div>
                          </Link>

                          <div className="model-actions mt-4">
                            <button className="btn btn-block btn-sm btn-outline-danger">Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        )}
      </section>
    );
  }
}

export default LuisModels;
