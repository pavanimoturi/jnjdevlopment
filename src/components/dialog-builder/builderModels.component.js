import React from 'react'
import Moment from 'react-moment';
import { Link, withRouter } from 'react-router-dom'
import { syncFetchAPICall, syncFetchAPICallDev, syncFetchDialogAPICall, syncFetchIntentsAPICall } from '../services';
import ChatWidget from '../chatWidget'

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Progress
} from 'reactstrap';

//Styles
import './builderModels.css';

// get models from models.js and assign
//to be replaced with the API call
import models from './models';


class BuilderModels extends React.Component {

  constructor(props, history) {

    super(props);
    this.state = {
      deleteInd: undefined,
      modal: false,
      modalDel: false,
      creatingDialog: false,
      custom: '',
      simulatedProgress: 0,
      newDialogName: '',
      newDialogDescription: '',
      newDialogIntent: '',
      dialogModels: [],
      delDialogName: '',
      intents: []
    };
    this.getDialogs = this.getDialogs.bind(this)
    this.getIntents = this.getIntents.bind(this)
    this.toggle = this.toggle.bind(this);
    this.toggleDel = this.toggleDel.bind(this);
    this.addDelDialog = this.addDelDialog.bind(this);
    // this.state.dialogModels = models;

    this.emptyStep = {
      "type": "waterfall",
      "construct": "NAME_PROMPT",
      "intent": "Weather",
      "description": "dialog for getting order status",
      "name": "Checking Account",
    };
    this.getDialogs(this.state);
    this.getIntents();
  }


  getIntents = async () => {
    let res = await syncFetchIntentsAPICall('https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/78ed91da-f93f-471c-97f7-c068b82f50d7/versions/0.1/examples')
    var resJSON = JSON.parse(res)
    // console.log('response from IntentAPI', resJSON)
    var intentData = [];
    var i = 0;
    this.selectOptionsList = [];
    resJSON.forEach((response) => {
      // console.log('-->', response.intentLabel)
      intentData.push(response.intentLabel)
    })
    var uniqueList = [...new Set(intentData)]

    uniqueList.forEach((listItem) => {
      i++
      this.selectOptionsList.push({
        key: i,
        intent: listItem
      })
    })
    console.log('intents list', this.selectOptionsList)
    // intents: []
    this.setState(prevState => ({ intents: this.selectOptionsList }))

  }

  getDialogs = async () => {
    let res = await syncFetchDialogAPICall('https://jnjchatbot.azurewebsites.net/dialogs');
    // console.log('--->', JSON.parse(res)
    // this.state.dialogModels = JSON.parse(res);
    var resJSON = JSON.parse(res)
    console.log('response from API', resJSON)
    resJSON = resJSON.filter((obj, index) => {
      if (Object.keys(obj).length > 0) {
        return obj.name.length > 0
      }
    })
    this.setState(prevState => ({ dialogModels: resJSON }))
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggleDel() {
    this.setState(prevState => ({
      modalDel: !prevState.modalDel
    }));
  }

  handleInputChange = (event) => {
    // console.log(event.target.value);
    let inputName = event.target.name;
    this.setState({ [inputName]: event.target.value })
  }

  createAndRedirect(numba) {

    this.setState(prevState => ({ creatingDialog: true }));
    var width = this.state.simulatedProgress;
    var interval = setInterval(() => {
      if (width >= 100) {
        clearInterval(interval);
        this.setState(prevState => ({
          simulatedProgress: 0,
          creatingDialog: false,
        }));

        this.props.history.push({
          pathname: '/dialog-builder/' + this.state.name,
          state: {
            new: true,
            name: this.state.name,
            description: this.state.description,
            intent: this.state.intent,
            id: 'abc',
            steps: [
              {
                construct: "NAME_PROMPT",
                name: "",
                promptOptions: { prompt: "" },
                typeValue: "nonAPI"
              }
            ]
          }
        });

      } else {
        width += 0.75;
        this.setState(prevState => ({ simulatedProgress: width }));
      }
    }, 10);

  }

  addModel() {
    return <Redirect to='/dialog-builder/Credit Information' />
  }

  dialogModelSelect(modelName) {
    console.log('inside select', modelName)
    console.log('inside create and redirect', this.state.dialogModels)
    var dialogModels = this.state.dialogModels
    var stepsData = {}
    var apiStep
    dialogModels.forEach((element) => {
      if (element.name == modelName) {
        stepsData = element
        apiStep = stepsData.steps[stepsData.steps.length - 1]['body']
        console.log('--->apiStep', apiStep)
      }
    })

    // stepsData.steps.pop();

    console.log('--->stepsData', stepsData)
    this.props.history.push({
      pathname: '/dialog-builder/' + modelName,
      state: {
        new: false,
        name: stepsData.name,
        description: stepsData.description,
        intent: stepsData.intent,
        steps: stepsData.steps,
        apiIntegrationSteps: apiStep
      }
    });
  }

  deleteDialog(name) {

    console.log('Deleting dialog: ', name)

    let url = 'https://jnjchatbot.azurewebsites.net/dialogs/'

    url = url + name

    async function api() {
      const response = await fetch(url, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        credentials: 'omit', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        }
      });

      // console.log("Await Response: ");
      let res = await response.text()
      res = JSON.stringify(res, null, 4);
      console.log("Await Response: ", res);
    }

    api()

    this.toggleDel()
    let mutatedModels = this.state.dialogModels;
    mutatedModels.splice(this.state.deleteInd, 1);
    this.setState(() => ({ dialogModels: mutatedModels }))

  }

  addDelDialog(name, index) {

    this.setState(prevState => ({ delDialogName: name }));
    this.setState(prevState => ({ deleteInd: index }));
    // this.state.delDialogName = name
    // this.state.deleteInd= index
    this.toggleDel()
  }

  render() {

    // console.log("Rendered", this.state);
    return (<section className="luis-models">
      <div className="container mt-5">
        <div>
          <div className="section-name-wrapper weight-700">
            Dialog Models
            <span className="weight-400">
              &nbsp;and Builder
            </span>
          </div>
          <div className="subtext">
            Build a bot dialog model and integrate it with your APIs.
          </div>
          <div className="separator mt-4 w-25">
            <hr />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="create-new-button-row mt-4">
          <button className="btn btn-outline-secondary button-primary" onClick={this.toggle}>
            <span className="fa fa-fw fa-plus"></span>&nbsp; Create New Dialog Model
          </button>
        </div>
        <div className="mt-4">
          <div className="list-headers">
            <div className="row models-row mb-2">
              {
                this.state.dialogModels.map((model, index) => (
                  <div className="col-4 mb-4" key={index}>
                    <div className="card model-card h-100">
                      <div className="card-body">
                        {/* <Link to={`/dialog-builder/${model.name}`}> */}
                        <div className="card-body-inner pointer" onClick={(e) => { this.dialogModelSelect(model.name) }}>
                          <div className="extra-small uppercase text-extra-muted scenario-number weight-400">
                            Dialog {index + 1}
                          </div>
                          <div className="model-name weight-700 pointer smoothen-transition">
                            <span>{model.name}</span>
                          </div>
                          <div className="w-50">
                            <hr />
                          </div>
                          <div className="model-info">
                            <div className="row mb-3">
                              <div className="col-3">
                                <strong className="text-extra-muted small weight-600">Steps</strong>
                                <div className="text-center w-50">
                                  <strong className="strong">{model.steps.length}</strong>
                                </div>
                              </div>
                              <div className="col-2">
                                <strong className="text-extra-muted small weight-600">API</strong>
                                <div>
                                  <span className="fa fa-fw fa-check-circle-o text-success"></span>
                                </div>
                              </div>
                              <div className="col-7">
                                <strong className="text-extra-muted small weight-600">Triggering Intent</strong>
                                <div>{model.intent}</div>
                              </div>

                            </div>
                          </div>
                          <div className="model-info model-description">
                            <strong className="text-extra-muted small weight-600">Description</strong>
                            <div>
                              {model.description}
                            </div>
                          </div>
                          <div className="model-info mt-2" hidden="hidden">
                            <strong className="text-extra-muted small weight-600">Last Updated</strong>
                            <br />
                            <Moment format="MMM DD, YYYY hh:mm A">
                              {model.updatedOn}
                            </Moment>
                          </div>
                        </div>
                        {/* </Link> */}
                        <div className="mb-3"></div>
                        <div className="model-actions mt-4">
                          <button className="btn btn-block btn-sm btn-outline-danger" onClick={() => { this.addDelDialog(model.name, index) }}>Delete</button>
                        </div>
                      </div>
                    </div>
                  </div>))
              }
            </div>
          </div>

        </div>
      </div>
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size="">
          <ModalHeader toggle={this.toggle} hidden="hidden">Modal title</ModalHeader>
          <ModalBody>
            <div className="card-body">
              <div className="modal-title weight-700">
                Create a
                <span className="weight-400">
                  &nbsp;New Dialog
                </span>
              </div>
              <div className="separator mt-3 w-25">
                <hr />
              </div>

              <div className="content mt-4">
                <div className="step">
                  <div className="lable weight-700">Step 1</div>
                  <div className="form-group">
                    <label htmlFor="inputAddress2">Let's start by naming your dialog</label>
                    <input type="text" name="name" className="form-control" id="inputAddress2" placeholder="A catchy dialog name" onChange={this.handleInputChange} />
                  </div>
                </div>

                <div className="step">
                  <div className="lable weight-700 mt-3">Step 2</div>
                  <label htmlFor="inputAddress2">Tell your fellow users what this dialog is about</label>
                  <textarea className="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="Short description of your dialog" onChange={this.handleInputChange}></textarea>
                </div>

                <div className="step">
                  <div className="lable weight-700 mt-3">Step 3</div>
                  <div className="form-group">
                    <div className="form-group ">
                      <label htmlFor="inputAddress2">Choose an Intent that will trigger this dialog</label>
                      <select className="form-control" name="intent" id="exampleFormControlSelect1" onChange={this.handleInputChange}>
                        {/* <option value="BookFlight">BookFlight</option>
                        <option value="CreateDelivery">CreateDelivery</option>
                        <option value="SalesOrderStatus">SalesOrderStatus</option>
                        <option value="SalesOrderFlow">SalesOrderFlow</option>
                        <option value="SalesOrderRelease">SalesOrderRelease</option> */}
                        {

                          this.state.intents.map((option) => {
                            return <option key={option.key} value={option.intent}>{option.intent}</option>
                          })
                        }
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div className="creation-progress mt-5">

                {
                  this.state.creatingDialog && <div>
                    <div className="mb-2 text-center">
                      Creating a new Dialog
                      </div>
                    <Progress color="success" className="creation-progressbar" value={this.state.simulatedProgress} />
                  </div>
                }
              </div>

              <div className="modal-inner-footer text-right mt-5">
                <Button className="btn btn-secondary" onClick={() => this.createAndRedirect(1)}>Next</Button>{' '}
                <Button className="btn btn-danger" onClick={this.toggle}>Cancel</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>

      <div>
        <Modal isOpen={this.state.modalDel} toggle={this.toggleDel} className={this.props.className} size="">
          <ModalBody>
            <div className="card-body">
              <div className="modal-title weight-600">
                This operation cannot be undone.
              </div>
              <span className="weight-400">
                &nbsp;Sure you want to Delete this Dialog?
              </span>

              <div className="separator mt-4 w-100">
                <hr />
              </div>

              <div className="modal-inner-footer text-right mt-2">
                <Button className="btn btn-outline-danger button-danger" onClick={() => this.deleteDialog(this.state.delDialogName)}>Delete</Button>{' '}
                <Button className="btn btn-outline-primary" onClick={this.toggleDel}>Cancel</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>

      <ChatWidget />

    </section>

    )

  }

  componentDidMount() {
    // this.setState(prevState => ({ dialogModels: models }));
  }

  componentWillUnmount() {
    console.log("Component is unmounting");
  }
}
export default BuilderModels
