const qnaEndpointHostName="https://westus.api.cognitive.microsoft.com/qnamaker/v4.0/knowledgebases/";
const qnaHeader = {
    "Ocp-Apim-Subscription-Key": "b4fc92a3d56d49d4bef20722fdcd94d0"
};

export function getModels() {
    let options = {
        headers: qnaHeader,
    };
    return fetch(qnaEndpointHostName, options);
}

export function getScenarioQASets(knowledgebaseId) {
    let options = {
        headers: qnaHeader,
    };
    return fetch(qnaEndpointHostName + knowledgebaseId + '/test/qna', options);
}
