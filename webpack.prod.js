const presetEnv = require.resolve('@babel/preset-env');
const presetReact = require.resolve('@babel/preset-react');
const classPropPlugin = require.resolve('@babel/plugin-proposal-class-properties');

module.exports = {
    mode: "production",
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [presetEnv, presetReact],
                        plugins: [classPropPlugin]
                    }
                }
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name(fileName) {
                                return `.${fileName}`;
                            },
                            emitFile: false
                        }
                    }
                ]
            }
        ]
    },
};